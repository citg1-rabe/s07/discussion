 mysql -u root -p


 --Adding a Record (create)
 --syntax: INSERT INTO tabel_name(col_name) VALUES (value1);

INSERT INTO artists(name) VALUES ("Incubus");
INSERT INTO artists(name) VALUES("Psy");

--Show all records
	-- Syntax: SELECT col_name from table_name;
	--SELECT * FROM table_name;

SELECT * FROM artists;	

--Adding a Record with multiple columns.

INSERT INTO albums(album_title, date_released, artist_id) VALUES ("Make Yourself", "1999-10-26", 1);
    INSERT INTO albums(album_title, date_released, artist_id) VALUES ("Psy 6", "2012-1-15", 2);

--Adding multiple records using one query.

INSERT INTO songs (song_name, length, genre, album_id) VALUES ("Gangnam Style", 253, "K-POP", 2), ("Drive", 153, "Rock", 1), ("Pardon Me", 223, "Rock", 1);

-- Show records that meet a certain condition.
	--WHERE clause
	-- SELECT column_name FROM table_name WHERE condtion.


SELECT song_name FROM songs WHERE genre = "Rock";	
SELECT song_name FROM songs WHERE genre = "rock";
SELECT song_name,length FROM songs WHERE length > 223;		
SELECT song_name, length from songs WHERE length >= "00:02:23";


--Updating records (update)
	-- add a record to update
	INSERT INTO songs( song_name,length,genre, album_id) VALUES ("Megalomaniac",410, " Classical",2);

-- UPDATING a record
	-- Syntax: Update tbl_name SET 
UPDATE songs SET song_name = "Stellar" WHERE song_name = "Megalomaniac";


--Updating multiple columns of the records
	--Syntax: UPDATE table_name SET column_name = new_value,col_name2 = new_value2 WHERE condition.

UPDATE songs SET length = 200, genre = "Rock", album_id = 1 WHERE song_name = "Stellar";

-- DELETING a record
	-- Syntax:  DELETE FROM tbl_name WHERE condition.

	DELETE FROM songs WHERE genre = "Rock" AND length < 200;